import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DispatcherService {

  private _settingsSubject = new ReplaySubject<object>();
  private _textSubject = new ReplaySubject<string>();

  settingPublished$ = this._settingsSubject.asObservable();
  textPublished$ = this._textSubject.asObservable();

  publishSettings(settings: object) {
    this._settingsSubject.next(settings);
  }

  publishText(text: string) {
    this._textSubject.next(text);
  }
}
