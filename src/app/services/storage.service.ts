import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  getText() {
    return localStorage.getItem('text');
  }

  setText(text:string) {
    localStorage.setItem('text', text);
  }

  setSetting(key:string, value:any) {
    const settings = JSON.parse(localStorage.getItem('settings') || '{}');
    const entry = { [key]: value };
    const new_settings = { ...settings, ...entry };
    localStorage.setItem('settings', JSON.stringify(new_settings))
  }

  getSetting(key:string) {
    const settings = localStorage.getItem('settings');
    if (!settings) {
      return null;
    }
    return JSON.parse(settings)[key];
  }

  getSettings() {
    return JSON.parse(localStorage.getItem('settings') || 'null');
  }
}
