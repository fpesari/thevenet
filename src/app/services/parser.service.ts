import { Injectable } from '@angular/core';

import { parse } from 'yaml';

@Injectable({
  providedIn: 'root'
})
export class ParserService {
  load(text:string):Array<[string, number]> {
    if (!text) { return [] }
    return Object.entries(parse(text));
  }
}
