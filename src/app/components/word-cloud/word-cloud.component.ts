import { Component, OnInit } from '@angular/core';

import { DispatcherService } from 'src/app/services/dispatcher.service';
import { ParserService } from 'src/app/services/parser.service';
import { StorageService } from 'src/app/services/storage.service';

import rgbaToRgb from 'rgba-to-rgb';

@Component({
  selector: 'app-word-cloud',
  templateUrl: 'word-cloud.component.html',
  styleUrls: ['word-cloud.component.scss'],
  providers: [ParserService, StorageService]
})
export class WordCloudComponent implements OnInit {
  items:Array<[string, number]> = [];
  settings:any = {};

  public constructor(
    private _dispatcherService:DispatcherService,
    private _parserService:ParserService,
    private _storageService:StorageService) {
    this._dispatcherService.textPublished$.subscribe(text => {
      this.items = this._parserService.load(text);
    });
    this._dispatcherService.settingPublished$.subscribe(settings => {
      this.settings = settings;
    });
  }

  ngOnInit(): void {
    this.items = this._parserService.load(
      this._storageService.getText() || '[]');
    this.settings = this._storageService.getSettings();
  }

  rgbize(weight: number):string {
    return rgbaToRgb('rgb(255, 255, 255)', 'rgba(' + (
      this.settings?.color || '255, 0, 0') + ', ' +
      Math.min(weight, 5) / 5 + ')');
  }

  limit(weight?: number):number {
    return Math.min(weight || 5, 5);
  }
}
