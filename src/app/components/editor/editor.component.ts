import { Component, OnInit } from '@angular/core';

import { DispatcherService } from 'src/app/services/dispatcher.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-editor',
  templateUrl: 'editor.component.html',
  styleUrls: ['editor.component.scss'],
  providers: [StorageService]
})
export class EditorComponent implements OnInit {
  public constructor(
    private _dispatcherService:DispatcherService,
    private _storageService:StorageService) {}

    text = `---
# Format for each entry:
#
# skill: level
# <str>  <int>
#
# by default, levels go from 1 to 5.
#
# An example follows.

C++: 2
CSS: 2
JavaScript: 5
HTML: 5
Haskell: 1
PHP: 4
`

  ngOnInit(): void {
    this.text = this._storageService.getText() || this.text;
    this._dispatcherService.publishText(this.text);
  }

  commit(): void {
    this._storageService.setText(this.text);
    this._dispatcherService.publishText(this.text);
  }
}
