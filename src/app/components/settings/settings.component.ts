import { Component, OnInit } from '@angular/core';

import { DispatcherService } from 'src/app/services/dispatcher.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-settings',
  templateUrl: 'settings.component.html',
  styleUrls: ['settings.component.scss'],
  providers: [StorageService]
})
export class SettingsComponent implements OnInit {
  color = 'rgb(203, 19, 19)';
  fontSize = 15;

  public constructor(
    private _dispatcherService:DispatcherService,
    private _storageService:StorageService) {}

  ngOnInit():void {
    this.color = this._storageService.getSetting('color') ?
     'rgb(' + this._storageService.getSetting('color') + ')' : this.color;
    this.fontSize = JSON.parse(
      this._storageService.getSetting('fontSize') || 'null') || this.fontSize;
    this._dispatcherService.publishSettings(
      { color: this.innerColor(this.color), fontSize: this.fontSize });
  }

  commitColor():void {
    this._storageService.setSetting('color', this.innerColor(this.color));
    this._dispatcherService.publishSettings(
      this._storageService.getSettings());
  }

  commitFontSize():void {
    this._storageService.setSetting('fontSize', this.fontSize);
    this._dispatcherService.publishSettings(
      this._storageService.getSettings());
  }

  private innerColor(rgb:string):string {
    return rgb.split('(')[1].slice(0, -1);
  }
}
