import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';

// Third-party components

// Material
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';

// Other
import { ColorPickerModule } from 'ngx-color-picker';

// App components
import { AppComponent } from './app.component';
import { WordCloudComponent } from './components/word-cloud/word-cloud.component';
import { EditorComponent } from './components/editor/editor.component';
import { SettingsComponent } from './components/settings/settings.component';

// App services
import { DispatcherService } from './services/dispatcher.service';

@NgModule({
  declarations: [
    AppComponent,
    WordCloudComponent,
    EditorComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    NoopAnimationsModule,
    AppRoutingModule,
    FormsModule,
    MatButtonModule,
    MatIconModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatTabsModule,
    MatToolbarModule,
    ColorPickerModule,
  ],
  providers: [DispatcherService],
  bootstrap: [AppComponent]
})
export class AppModule { }
