# thevenet

thevenet is a web app which can be used to generate word clouds.
It was written for the purpose of generating a skill cloud for my CV.

thevenet is written using the Angular framework and by default, it does not
connect to any Google servers.

![Screenshot of thevenet](screenshot.png)

## Acknowledgements

I could not keep track of all people who indirectly helped me, especially by
sharing information on StackOverflow.

I want to thank Edoardo Midali for his excellent Angular course on Youtube,
which taught me the basics of Angular.

## License

All files in this repository, unless otherwise noted (if another FLOSS license
applies), are released under the GNU Affero General Public License, version 3
or any later version.

Since two fonts are bundled, here are their licenses.

- Pacifico
```
Copyright 2018 The Pacifico Project Authors
(https://github.com/googlefonts/Pacifico)

This Font Software is licensed under the SIL Open Font License, Version 1.1.
This license is available with a FAQ at: http://scripts.sil.org/OFL
```

- Inconsolata
```
Copyright 2006 The Inconsolata Project Authors

This Font Software is licensed under the SIL Open Font License, Version 1.1.
This license is available with a FAQ at: http://scripts.sil.org/OFL
```
